## Welcome to myfridge3

### A RESTful API example written in Clojure.

Check out the live Swagger [API Docs](/api/api-docs/)

See the source [myfridge3](https://gitlab.com/tmarble/myfridge3)
