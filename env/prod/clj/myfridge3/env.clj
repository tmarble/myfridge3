(ns myfridge3.env
  (:require [clojure.tools.logging :as log]))

(def defaults
  {:init
   (fn []
     (log/info "\n-=[myfridge3 started successfully]=-"))
   :stop
   (fn []
     (log/info "\n-=[myfridge3 has shut down successfully]=-"))
   :middleware identity})
