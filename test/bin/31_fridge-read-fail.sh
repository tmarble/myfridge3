#!/bin/sh
# fridge-read-fail

set -e

api="$URL/api/fridge"

# construct curl args
args=""
args="$args -s" # NOTE turning off silent mode is useful for debugging
args="$args -m 5" # don't wait more than 5 seconds

accept="Accept: application/json"
body="$RESULTS/$NUMBER.json"
response="$RESULTS/$NUMBER.http_code"
# NOTE: the %{stderr} option only available in recent versions of curl
# format="%{stderr}%{http_code}\n" # write response to stderr
format="\nHTTP_CODE=%{http_code}\n" # write response to stdout

name="sriracha"
params="?name=$name"

curl $args --header "$accept" \
     -w "$format" "$api$params"

# NOTE: the %{stderr} option only available in recent versions of curl
#     -w "$format" -d @$body $api 2> $response
awk -F= '/^HTTP_CODE/ { print $2; }' $OUT > $response

if [ -r "$response" ]; then
    http_code="$(cat $response)"
    if [ "$http_code" != "404" ]; then
        echo "expected 404 - Not Found, instead got http_code: $http_code"
        exit 1
    fi
else
    echo "curl did not provide an http_code"
    exit 1
fi
