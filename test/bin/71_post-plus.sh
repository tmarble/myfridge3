#!/bin/sh
# post-plus

set -e

api="$URL/api/math/plus"

# construct curl args
args=""
args="$args -s" # NOTE turning off silent mode is useful for debugging
args="$args -m 5" # don't wait more than 5 seconds

content="Content-Type: application/json"
accept="Accept: application/json"
body="$RESULTS/$NUMBER.json"

cat << EOF > $body
{
    "x": 2
}
EOF

# disabled for now
# curl $args --header "$content" --header "$accept"  -d @$body $api
