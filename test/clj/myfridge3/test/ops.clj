(ns myfridge3.test.ops
  (:require [clojure.test :refer :all]
            [clojure.spec.test.alpha :as stest]
            [expound.alpha :as expound]
            [myfridge3.ops :refer :all]))

(defn spec-test [sym]
  (let [results (stest/check sym)
        pass? (-> results first :clojure.spec.test.check/ret :pass?)]
    (when-not pass?
      (expound/explain-results results))
    pass?))

;; (deftest plus-api
;;   (testing "plus"
;;     (is (true? (spec-test 'myfridge3.ops/plus)))))

(deftest fridge-api
  (testing "fridge-create"
    (is (true? (spec-test 'myfridge3.ops/fridge-create))))
  (testing "fridge-read"
    (is (true? (spec-test 'myfridge3.ops/fridge-read))))
  (testing "fridge-update"
    (is (true? (spec-test 'myfridge3.ops/fridge-update))))
  (testing "fridge-delete"
    (is (true? (spec-test 'myfridge3.ops/fridge-delete)))))
