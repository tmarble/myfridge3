(ns myfridge3.routes.services
  (:require [cheshire.core :as json] ;; FIXME
            [clojure.tools.logging :as log]
            [ring.util.http-response :refer :all]
            [reitit.swagger :as reitit-swagger]
            [reitit.swagger-ui :as swagger-ui]
            [reitit.ring.coercion :as ring-coercion]
            [reitit.coercion.spec :as spec-coercion]
            [reitit.ring.middleware.muuntaja :as muuntaja]
            [reitit.ring.middleware.parameters :as parameters]
            [myfridge3.specs :as specs]
            [myfridge3.ops :as ops]
            [myfridge3.middleware.formats :as formats]
            [myfridge3.middleware.exception :as exception]))

;; response has status, optionally message, results
(defn respond [response]
  (let [{:keys [status message results]} response]
    {:status status
     :headers {"Content-Type" "application/json"}
     :body (json/generate-string
            (cond-> {:status status}
              message (assoc :message message)
              results (assoc :results results))
           {:escape-non-ascii true})
     }))

(defn log-request-handler [handler]
  (fn [req]
    (let [v (handler req)]
      (log/info "REQUEST" (:request-method req) (:uri req)
                "FROM" (:remote-addr req)
                "=> RESPONSE" (:status v))
      v)))

(def log-request-middleware
  {:name ::log-request
   :wrap log-request-handler})

(defn service-routes []
  ["/api"
   {:coercion spec-coercion/coercion
    :muuntaja formats/instance
    :swagger {:id ::api}
    :middleware [log-request-middleware
                 ;; query-params & form-params
                 parameters/parameters-middleware
                 ;; content-negotiation
                 muuntaja/format-negotiate-middleware
                 ;; encoding response body
                 muuntaja/format-response-middleware
                 ;; exception handling
                 exception/exception-middleware
                 ;; decoding request body
                 muuntaja/format-request-middleware
                 ;; coercing response bodys
                 ring-coercion/coerce-response-middleware
                 ;; coercing request parameters
                 ring-coercion/coerce-request-middleware]}

   ;; swagger documentation
   ["" {:no-doc true
        :swagger {;; :basePath "/api"
                  :info {:title "myfridge3"
                         :version "1.0.0"
                         :description "RESTful API example in Clojure <a href=\"/api/swagger.json\">swagger.json</a>"
                         :contact {:name "Tom Marble"
                                   :email "tmarble@gmail.com"
                                   :url "https://gitlab.com/tmarble/myfridge3"}
                         :license {:name "Apache License 2.0"
                                   :url "http://opensource.org/licenses/Apache-2.0"}}
                  }
        }

    ["/swagger.json"
     {:get (reitit-swagger/create-swagger-handler)}]

    ["/api-docs/*"
     {:get (swagger-ui/create-swagger-ui-handler
             {:url "/api/swagger.json"
              :config {:validator-url nil}})}]]

   ["/ping"
    {:get {:summary "simple ping test"
           :handler (constantly (ok {:message "pong"}))}}]

   ;; ["/math"
   ;;  {:swagger {:tags ["math"]}}

   ;;  ["/plus"
   ;;   {:get {:summary "plus with query parameters"
   ;;          :parameters {:query ::specs/plus-params}
   ;;          :handler (fn [{{{:keys [x y] :or {y 100}} :query} :parameters}]
   ;;                     (respond (ops/plus x y)))}

   ;;    :post {:summary "plus with body parameters"
   ;;           :parameters {:body ::specs/plus-params}
   ;;           :handler (fn [{{{:keys [x y] :or {y 100}} :body} :parameters}]
   ;;                      (respond (ops/plus x y)))}
   ;;    }]]

   ["/fridge"
    {:swagger {:tags ["fridge"]}
     :post {:summary "create fridge item"
            :parameters {:body ::specs/fridge-create}
            :handler (fn [{{params :body} :parameters}]
                       (respond (ops/fridge-create params)))}

     :get {:summary "read fridge item"
           :parameters {:query ::specs/fridge-read}
           :handler (fn [{{params :query} :parameters}]
                      (respond (ops/fridge-read params)))}

     :put {:summary "update fridge item"
           :parameters {:body ::specs/fridge-update}
           :handler (fn [{{params :body} :parameters}]
                      (respond (ops/fridge-update params)))}

     :delete {:summary "delete fridge item"
              :parameters {:body ::specs/fridge-delete}
              :handler (fn [{{params :body} :parameters}]
                         (respond (ops/fridge-delete params)))}}]])
