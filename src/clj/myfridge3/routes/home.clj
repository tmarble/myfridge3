(ns myfridge3.routes.home
  (:require [clojure.java.io :as io]
            [ring.util.http-response :as response]
            [myfridge3.layout :as layout]
            [myfridge3.middleware :as middleware]))

(defn home-page [request]
  (layout/render request "home.html"
                 {:docs (-> "docs/docs.md" io/resource slurp)}))

(defn about-page [request]
  (layout/render request "about.html"))

(defn home-routes []
  [""
   {:middleware [middleware/wrap-csrf
                 middleware/wrap-formats]}
   ["/" {:get home-page}]
   ["/about" {:get about-page}]])
